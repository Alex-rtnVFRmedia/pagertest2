# control the Arduino RF22 pager TX
# modified ON1ARF code
#
# 2017-06-23 Alex The Engineer
# The Rat's Nest, Ipswich UK
#
#
import serial
import pprint
import sys
from colorconsole import terminal
from colored import fg, bg, attr
import readline

# global colours

res=attr('reset')

# tty open now coded as a function
#
# colored module used here instead of colorconsole/screen object
# just in case there isn't a global screen object available 
#

def page(ttydev,capcode,function,repeats,message):
#
# create msg bytearray (NB: there should be more error
# checking here
#
  msg=(b'P ')
  msg+=(b'%07d' % capcode)+(b' ')
  msg+=(b'%01d' % function)+(b' ')
  msg+=(b'%01d' % repeats)+(b' ')
  msg+=(message)+(b'\r')

  print (fg(80),bg(0x12),'** opening tty:',ttydev+res)
 
# serial port open with error checking this time
  try:
    ser = serial.Serial(ttydev, timeout=3)
  except serial.SerialException:
    print(res)
    sys.exit("** ERROR: cannot open tty\n\n")
  
# if we got here everything is OK
# RTS/DTR does not actually need to be flipped
# as opening the tty does that anyway
# which saves some time starting up the arduino comms
# ser.setRTS(True)
# ser.setRTS(False)
#
# get the lines from the buffer
  print (fg(0x0b),bg(0x15),'** tty open OK: >>check TX<<'+res)
  
  ibuf = ser.readlines()
  print (fg(202))
  #pprint.pprint(ibuf)
  print (' ibuf[0]  :',ibuf[0])
  print (' ibuf[-1] :',ibuf[-1])
  
# check to see what came down comms line is expected
  if ibuf[0] != (b'RF22 init OK\r\n'):
  	print(res)
  	sys.exit("** ERROR: RF22 init error\n\n")
  if ibuf[-1] != (b'F <freqmhz> <freq100Hz>\r\n'):
	  print(res)
	  sys.exit("** ERROR: RF22 comms error\n\n")
# if TX has correctly initialised now send message 
  ser.write(msg)
# get a line back from TTY, it should be exactly same as msg 
# but with a \n after it
# if it hasn't something has gone wrong
  check=ser.readline()
  
  print(fg(0x50),'<< from tty:',fg(0x72),bg(0x08),check,res,fg(0x52))
  if check != (msg+b'\n'):
	  print(res)
	  sys.exit("** ERROR: pager comms error\n\n")
  print (' ++ comms to pager TX OK ++')	
#  get data returned over comms link 
#  and now echo it with some pretty colours :)
  colindex=0x70	
  while True:
    data=ser.readline()
# only print it if a sending confirmation
    if data==(b'POCSAG SEND\r\n'):
      print (fg(0x30),'<< from tty:',fg(colindex),data)
      colindex += 1

    if data==(b'F <freqmhz> <freq100Hz>\r\n'):
      break
  # all done, close tty
  print (fg(0x28),'** message sent correctly **\n')
  ser.close()


def header(scr):
  scr = terminal.get_terminal(conEmu=False)
  scr.clear()
  scr.xterm256_set_fg_color(0x52)
  scr.xterm256_set_bk_color(0x08)
  scr.print_at(2,0,"rtn telecom : pagertest 1.00")
  scr.xterm256_set_bk_color(0)
  scr.gotoXY(2,3)
  scr.xterm256_set_fg_color(0x20)
  print('capcode:')
  scr.xterm256_set_bk_color(0x08)
  scr.xterm256_set_fg_color(0x78)
  scr.gotoXY(10,3)
  print('        ')
  scr.xterm256_set_bk_color(0)
  scr.gotoXY(20,3)
  scr.xterm256_set_fg_color(0x20)
  print('alert:')
  scr.xterm256_set_bk_color(0x08)
  scr.xterm256_set_fg_color(0x78)
  scr.gotoXY(26,3)
  print(' ')
  scr.xterm256_set_bk_color(0)
  scr.gotoXY(2,5)
  scr.xterm256_set_fg_color(0x20)
  print('message:')
  scr.xterm256_set_bk_color(0x08)
  scr.xterm256_set_fg_color(0x78)
  scr.gotoXY(10,5)
  print('                                        ')
  scr.xterm256_set_bk_color(0)
  scr.gotoXY(0,9)
  scr.xterm256_set_fg_color(0x28)
# to be developed, input capcode  
def get_capcode(scr,default):
  pass
# display capcode in input box 
def disp_capcode(scr,capcode):
  scr.xterm256_set_bk_color(0x08)
  scr.xterm256_set_fg_color(0x78)
  scr.gotoXY(10,3)
  print (str((b'%07d' % capcode),'utf-8'))
  scr.xterm256_set_bk_color(0)
# display alert code in input box
def disp_alert(scr,alert):
  scr.xterm256_set_bk_color(0x08)
  scr.xterm256_set_fg_color(0x78)
  scr.gotoXY(26,3)
  print (str((b'%01d' % alert),'utf-8'))
  scr.xterm256_set_bk_color(0)
# display message in input box  
def disp_msg(scr,message):
  scr.xterm256_set_bk_color(0x08)
  scr.xterm256_set_fg_color(0x78)
  scr.gotoXY(10,5)
  print (str(message,'utf-8'))
  scr.xterm256_set_bk_color(0)
  scr.gotoXY(0,9)
#
# main code now starts here
#
# initialise the screen
#
# 2017-06-23: now accepts interactive input of message
#
# a blank line transmits the hardcoded message
#
# 'q' exits the code
#
while True:

# refresh the header
	screen = terminal.get_terminal(conEmu=False)
	header(screen)
# message now split into capcode, function (0-3), repeats
# and 40 character bytearray
#
# hardcoded values
#
	capcode = 20008
	function = 2
	repeats = 1
	message = b'rtn: miauw naar de maan!<<<pagertest>>>'
	ttydev="/dev/ttyACM0"
# display the capcode and alert that are currently hardcoded
# but leave the msg box blank as it now accepts user input
#
	disp_capcode(screen,capcode)
	disp_alert(screen,function)
#
# create a quick and dirty coloured prompt for the input	
#
	screen.gotoXY(2,7)
	screen.xterm256_set_fg_color(0x38)
	print('------>')
	screen.gotoXY(10,7)
	screen.xterm256_set_fg_color(0x68)
#
# this uses readline so has some basic editing functions
#
# anything over 40 character is clipped
#	
	ms1=input()
	if ms1 !='':
		message=ms1[0:40].encode('utf-8')
	if ms1 =='q':
		break
#
# display the msg in the top box and transmit it
#
	disp_msg(screen,message)
	page(ttydev,capcode,function,repeats,message)
#
# terminate script and reset all the console colours
#
print (fg(0xc8),'\n++ Script completed sucessfully ++\n',res)


